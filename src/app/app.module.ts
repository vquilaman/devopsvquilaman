import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { CampaniaPage } from '../pages/campania/campania';
import { AboutDailyPage } from '../pages/aboutDaily/aboutDaily';
import { AboutPlanningPage } from '../pages/aboutPlanning/aboutPlanning';
import { AboutReviewPage } from '../pages/aboutReview/aboutReview';
import { AboutRetroPage } from '../pages/aboutRetro/aboutRetro';
import { Daily01Page } from '../pages/daily/daily01';
import { FormDaily01Page } from '../pages/formDaily/formDaily01';
import { FormExamplePage } from '../pages/formExample/formExample';
import { FinCampaniaDailyPage } from '../pages/finCampania/finCampaniaDaily';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    CampaniaPage,
    AboutDailyPage,
    AboutPlanningPage,
    AboutReviewPage,
    AboutRetroPage,
    Daily01Page,
    FormDaily01Page,
    FormExamplePage,
    FinCampaniaDailyPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    CampaniaPage,
    AboutDailyPage,
    AboutPlanningPage,
    AboutReviewPage,
    AboutRetroPage,
    Daily01Page,
    FormDaily01Page,
    FormExamplePage,
    FinCampaniaDailyPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
