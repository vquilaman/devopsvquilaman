import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Daily01Page } from '../daily/daily01';

@Component({
  selector: 'page-aboutPlanning',
  templateUrl: 'aboutPlanning.html'
})
export class AboutPlanningPage {

  constructor(public navCtrl: NavController, params: NavParams) {
      console.log("VQD #************ Llegue a aboutPlanning.ts *****************");
      console.log("VQD #************ item.accion -> " + params.data.item.accion);
  }
    
  openPagePlanning(){
      console.log("VQD #************ Llegue a aboutPlanning.ts *****************");
      this.navCtrl.push(Daily01Page);
  }

}
