import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CampaniaPage } from '../campania/campania';

@Component({
  selector: 'page-finCampaniaDaily',
  templateUrl: 'finCampaniaDaily.html'
})
export class FinCampaniaDailyPage {

  items = [];
    
  constructor(public navCtrl: NavController, params: NavParams) {
      console.log("VQD #************ Llegue a finCampaniaDaily.ts *****************");
      console.log("VQD #************ ¡Misión completada! ***************** ");
      console.log("VQD #************ ¡Felicitaciones! haz cumpletado con éxito la misión ***************** ");
      console.log("VQD #************ params option -> " + params.data);
      if(params.data === 4){
          this.items = [
            {
              'title': 'Misión completada',
              'subtitle': 'Felicitaciones! haz cumpletado con éxito la misión',
              'img': 'trophy.png'
            }
            ]
      } else {
          this.items = [
            {
              'title': 'Misión no completada',
              'subtitle': 'Respuesta incorrecta, vuelve a intentarlo',
              'img': 'mision_fallida.jpg'
            }
            ]
          }
      }
      
    
  openPageInicio(){
      console.log("VQD #************ Llegue a finCampaniaDaily.ts *****************");
      this.navCtrl.push(CampaniaPage);
  }

}
