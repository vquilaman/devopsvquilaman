import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormExamplePage } from './formExample';

@NgModule({
  declarations: [
    FormExamplePage,
  ],
  imports: [
    IonicPageModule.forChild(FormExamplePage),
  ],
})
export class ExamplePageModule {}
