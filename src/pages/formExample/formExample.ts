import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-formExample',
  templateUrl: 'formExample.html'
})

export class FormExamplePage {
    myForm: FormGroup;

    constructor(
      public navCtrl: NavController,
      public formBuilder: FormBuilder
    ) {
      this.myForm = this.createMyForm();
    }
    
    saveData(){
      console.log(this.myForm.value);
    }
    
    private createMyForm(){
      console.log("VQD #************ Llegue a formExample.ts *****************");
      return this.formBuilder.group({
        name: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.required],
        dateBirth: ['', Validators.required],
        passwordRetry: this.formBuilder.group({
          password: ['', Validators.required],
          passwordConfirmation: ['', Validators.required]
        }),
        gender: ['', Validators.required],
      });
    }
}
