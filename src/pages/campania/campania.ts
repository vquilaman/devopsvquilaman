import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AboutDailyPage } from '../aboutDaily/aboutDaily';
import { AboutPlanningPage } from '../aboutPlanning/aboutPlanning';
import { AboutReviewPage } from '../aboutReview/aboutReview';
import { AboutRetroPage } from '../aboutRetro/aboutRetro';

@Component({
      templateUrl: 'campania.html'
})
    
export class CampaniaPage {
    
    items = [];

    constructor(public nav: NavController) {
      this.items = [
        {
          'title': 'Daily Meeting',
          'icon': 'angular',
          'accion': '1'
        },
        {
          'title': 'Sprint Planning',
          'icon': 'css3',
          'accion': '2'
        },
        {
          'title': 'Sprint Review',
          'icon': 'html5',
          'accion': '3'
        },
        {
          'title': 'Sprint Retrospective',
          'icon': 'javascript',
          'accion': '4'
        },
      ]
    }
    
    openNavDetailsPage(item) {
        console.log("VQD #************ Llegue a campania.ts *****************"); 
        switch (Number(item.accion)) {
            case 1:  this.nav.push(AboutDailyPage, { item: item });break;
            case 2:  this.nav.push(AboutPlanningPage, { item: item }); break;
            case 3:  this.nav.push(AboutReviewPage, { item: item }); break;
            case 4:  this.nav.push(AboutRetroPage, { item: item }); break;
            default: break;
        }
      }
}
