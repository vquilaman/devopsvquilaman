import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Daily01Page } from './daily01';

@NgModule({
  declarations: [
    Daily01Page,
  ],
  imports: [
    IonicPageModule.forChild(Daily01Page),
  ],
})
export class Daily01PageModule {}
