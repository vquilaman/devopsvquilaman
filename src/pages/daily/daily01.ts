import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormDaily01Page } from '../formDaily/formDaily01';

@Component({
  selector: 'page-daily01',
  templateUrl: 'daily01.html'
})


export class Daily01Page {
    
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
      console.log("VQD #************ Llegue a daily01.ts *****************");
      console.log('ionViewDidLoad Daily01Page');
    }
    
    openPageformDaily01(){
        console.log("VQD #************ Llegue a aboutDaily.ts *****************");
        this.navCtrl.push(FormDaily01Page);
    }
}
