import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Daily01Page } from '../daily/daily01';

@Component({
  selector: 'page-aboutDaily',
  templateUrl: 'aboutDaily.html'
})
export class AboutDailyPage {

  constructor(public navCtrl: NavController, params: NavParams) {
      console.log("VQD #************ Llegue a aboutDaily.ts *****************");
      console.log("VQD #************ item.accion -> " + params.data.item.accion);
  }
    
  openPageDaily(){
      console.log("VQD #************ Llegue a aboutDaily.ts *****************");
      this.navCtrl.push(Daily01Page);
  }

}
