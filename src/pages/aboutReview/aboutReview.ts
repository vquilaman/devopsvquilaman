import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Daily01Page } from '../daily/daily01';

@Component({
  selector: 'page-aboutReview',
  templateUrl: 'aboutReview.html'
})
export class AboutReviewPage {

  constructor(public navCtrl: NavController, params: NavParams) {
      console.log("VQD #************ Llegue a aboutReview.ts *****************");
      console.log("VQD #************ item.accion -> " + params.data.item.accion);
  }
    
  openPageReview(){
      console.log("VQD #************ Llegue a aboutReview.ts *****************");
      this.navCtrl.push(Daily01Page);
  }

}
