import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Daily01Page } from '../daily/daily01';

@Component({
  selector: 'page-aboutRetro',
  templateUrl: 'aboutRetro.html'
})
export class AboutRetroPage {

  constructor(public navCtrl: NavController, params: NavParams) {
      console.log("VQD #************ Llegue a aboutRetro.ts *****************");
      console.log("VQD #************ item.accion -> " + params.data.item.accion);
  }
    
  openPageRetro(){
      console.log("VQD #************ Llegue a aboutRetro.ts *****************");
      this.navCtrl.push(Daily01Page);
  }

}
