import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { CampaniaPage } from '../campania/campania';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = CampaniaPage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;

  constructor() {

  }
}
