import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormDaily01Page } from './formDaily01';

@NgModule({
  declarations: [
    FormDaily01Page,
  ],
  imports: [
    IonicPageModule.forChild(FormDaily01Page),
  ],
})
export class Daily01PageModule {}
