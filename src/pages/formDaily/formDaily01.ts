import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FinCampaniaDailyPage } from '../finCampania/finCampaniaDaily';

@IonicPage()
@Component({
  selector: 'page-formDaily01',
  templateUrl: 'formDaily01.html'
})

export class FormDaily01Page {
    myForm: FormGroup;

    constructor(
      public navCtrl: NavController,
      public formBuilder: FormBuilder
    ) {
      this.myForm = this.createMyForm();
    }
    
    saveData(){
      console.log(this.myForm.value.option);
      this.validarRespuesta(Number(this.myForm.value.option));
    }
    
    private createMyForm(){
        console.log("VQD #************ Llegue a formDaily01.ts *****************");
        return this.formBuilder.group({
          option: ['', Validators.required],
        });
      }
    
    private validarRespuesta(option:Number){
        this.navCtrl.push(FinCampaniaDailyPage, option);
    }
    
}
